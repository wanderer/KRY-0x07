﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x07
{
    public partial class main_form : Form
    {
        public main_form()
        {
            InitializeComponent();
        }

        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToArray();
        char[,] alphabet_square = new char[26,26];

        void fill_alph_square()
        {
            int linear_shift = 0;
            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < alphabet.Length; j++)
                {
                    alphabet_square[i, j] = alphabet[linear_shift % 26];
                    linear_shift++;
                }
                linear_shift++;
            }
        }

        void empty_message_warning()
        {
            MessageBox.Show("The size of the key is to be of the exact same length as the message (or shorter, in which case the key is repeated until it's as long as the message).\nNo message means nothing to incur key length from.","Error: empty message");
            return;
        }
        int check_message_and_key(TextBox m_tb, TextBox k_tb)
        {
            int status = 0;
            string message = m_tb.Text.ToUpper();
            string key = k_tb.Text.ToUpper();
            Match m_match = Regex.Match(message, @"\d|\s+");
            Match k_match = Regex.Match(key, @"\d|\s+");

            if (message == "")
            {
                empty_message_warning();
                status = -1;
            }
            else if (key == "")
            {
                MessageBox.Show("No key provided, nothing to do.","Error");
                status = -2;
            }
            else if (key.Length > message.Length)
            {
                MessageBox.Show("The key is to be shorter than or as long as the message", "Error: key longer than message");
                status = -3;
            }
            else if (m_match.Success)
            {
                MessageBox.Show("The message contains invalid characters.\nOnly alphabetic characters are allowed.", "Error: invalid message characters");
                status = -4;
            }
            else if (k_match.Success)
            {
                MessageBox.Show("The key contains invalid characters.\nOnly alphabetic characters are allowed.", "Error: invalid key characters");
                status = -5;
            }
            return status;
        }

        void prepare_key(TextBox tb_m, TextBox tb_k)
        {
            string message = tb_m.Text.ToUpper();
            string key = tb_k.Text.ToUpper();
            if (key.Length < message.Length)
            {
                int length_diff = message.Length - key.Length;
                for (int i = 0; i < length_diff; i++)
                {
                    key += key.Substring(i,1);
                }
                tb_k.Text = key;
            }
        }

        void encrypt()
        {
            fill_alph_square();
            string key = textBox1.Text.ToUpper();
            string message = textBox2.Text.ToUpper();
            int klength = key.Length;
            int mlength = klength;
            int[] m_ints = new int[mlength];
            int[] k_ints = new int[klength];
            string cryptmessage = "";

            for (int i =0; i < mlength; i++)
            {
                string mchar_to_deal_with = message.Substring(i, 1);
                string kchar_to_deal_with = key.Substring(i, 1);
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (mchar_to_deal_with == Convert.ToString(alphabet[j]))
                    {
                        m_ints[i] = j;
                    }
                    if (kchar_to_deal_with == Convert.ToString(alphabet[j]))
                    {
                        k_ints[i] = j;
                    }
                }
            }

            for (int i = 0; i < mlength; i++)
            {
                cryptmessage += alphabet[(m_ints[i] + k_ints[i]) % alphabet.Length];
            }
            textBox3.Text = cryptmessage;
        }

        void decrypt()
        {
            fill_alph_square();
            string key = textBox5.Text.ToUpper();
            string cryptmessage = textBox4.Text.ToUpper();
            int klength = key.Length;
            int mlength = cryptmessage.Length;
            int[] m_ints = new int[mlength];
            int[] k_ints = new int[klength];
            string message = "";

            for (int i = 0; i < mlength; i++)
            {
                string mchar_to_deal_with = cryptmessage.Substring(i, 1);
                string kchar_to_deal_with = key.Substring(i, 1);
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (mchar_to_deal_with == Convert.ToString(alphabet[j]))
                    {
                        m_ints[i] = j;
                    }
                    if (kchar_to_deal_with == Convert.ToString(alphabet[j]))
                    {
                        k_ints[i] = j;
                    }
                }
            }

            for (int i = 0; i < mlength; i++)
            {
                message += alphabet[(m_ints[i] - k_ints[i] + 26) % alphabet.Length];
            }
            textBox6.Text = message;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* encrypt */
            int status = check_message_and_key(textBox1, textBox2);
            if (status != 0)
            {
                return;
            }
            else
            {
                prepare_key(textBox1, textBox2);
                encrypt();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /* decrypt */
            int status = check_message_and_key(textBox4, textBox5);
            if (status != 0)
            {
                return;
            }
            else
            {
                prepare_key(textBox4,textBox5);
                decrypt();
            }
        }
    }
}
